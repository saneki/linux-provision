.PHONY: ${TARGETS}

all: install

install:
	ansible-playbook -i '127.0.0.1,' --ask-vault-pass provision.yml

setup:
	sudo pacman -Syu
	sudo pacman -S ansible
	mkdir -p library && cp 'modules/ansible-aur/aur' 'library/'
